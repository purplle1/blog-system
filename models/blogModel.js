const mongoose = require('mongoose');
const slugify = require('slugify');
const validator = require('validator');

const blogSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true,
        trim: true,
        minlength: [10, 'Minimum length should be 10'],
        maxlength: [40, 'Maximum length should be 40'],
    },
    slug: String,
    description: {
        type: String,
        required: true,
    },
    createdBy: {
        type: String,
    },
    createdAt: {
        type: Date,
        default: Date.now(),
        select: false,
    },
    lastUpdatedAt: Date,
    active: {
        type: Boolean,
        default: true,
        select: false,
    },
});

blogSchema.pre('save', function (next) {
    if (this.isModified('title') || this.isNew) {
        this.slug = slugify(this.title, { lower: true });
    }
    next();
});

blogSchema.pre(/^find/, function (next) {
    this.find({ active: { $ne: false } });
    next();
});

const Blog = mongoose.model('Blog', blogSchema);

module.exports = Blog;
