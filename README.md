# Blog System API

API made using NodeJS for a simple blog posting system.

## Installation

1. Clone the repository

```sh
git clone https://gitlab.com/purplle1/blog-system.git
```

2. Use the npm package manager to install the required packages.

```sh
npm install
```

3. Create a `config.env` file and add the following variables to it: 

* `NODE_ENV` = "development"
* `PORT` = Port number
* `DATABASE` = Database link
* `JWT_SECRET` = Secret string for encryption
* `JWT_EXPIRES_IN` = Expiration time for jwt token

## Usage

Run the project using the following command:

```sh
npm start
```
