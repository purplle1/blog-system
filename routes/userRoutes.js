const express = require('express');
const userController = require('./../controllers/userController');
const authController = require('./../controllers/authController');

const router = express.Router();

router.route('/signup').post(authController.signup);
router.route('/login').post(authController.login);

router.post('/forgotPassword', authController.forgotPassword);
router.patch('/resetPassword/:token', authController.resetPassword);

router.patch(
    '/updateMyPassword',
    authController.protect,
    authController.changePassword
);

router.get(
    '/',
    authController.protect,
    authController.restrictTo('admin'),
    userController.getAllUsers
);

router.get('/viewProfile', authController.protect, userController.viewProfile);

router.patch(
    '/updateProfile',
    authController.protect,
    userController.updateProfile
);

module.exports = router;
