const util = require('util');
const jwt = require('jsonwebtoken');
const User = require('./../models/userModel');
const AppError = require('./../utils/appError');
const sendEmail = require('./../utils/email');
const crypto = require('crypto');

const signToken = (id) => {
    return jwt.sign({ id: id }, process.env.JWT_SECRET, {
        expiresIn: process.env.JWT_EXPIRES_IN,
    });
};

const createSendToken = (user, statusCode, res) => {
    const token = signToken(user._id);

    const cookieOptions = {
        expires: new Date(
            Date.now() + process.env.JWT_COOKIE_EXPIRES_IN * 24 * 60 * 60 * 1000
        ),
        httpOnly: true,
    };

    if (process.env.NODE_ENV === 'production') cookieOptions.secure = true;

    res.cookie('jwt', token, cookieOptions);

    user.password = undefined;
    res.status(statusCode).json({
        status: 'success',
        token,
        data: {
            user: user,
        },
    });
};

exports.signup = async (req, res, next) => {
    try {
        const newUser = await User.create({
            name: req.body.name,
            email: req.body.email,
            password: req.body.password,
            passwordConfirm: req.body.passwordConfirm,
            passwordChangedAt: req.body.passwordChangedAt,
        });

        createSendToken(newUser, 201, res);
    } catch (err) {
        next(new AppError(err, 404));
    }
};

exports.login = async (req, res, next) => {
    try {
        const email = req.body.email;
        const password = req.body.password;

        if (!email || !password) {
            return next(
                new AppError('Please provide email and password!', 400)
            );
        }

        const user = await User.findOne({ email: email }).select('+password');
        console.log(user);
        if (!user || !(await user.correctPassword(password, user.password))) {
            return next(new AppError('Incorrect email or password!', 401));
        }

        createSendToken(user, 200, res);
    } catch (err) {
        next(new AppError(err, 404));
    }
};

exports.protect = async (req, res, next) => {
    try {
        //Check for token and extract it

        let token;
        if (
            req.headers.authorization &&
            req.headers.authorization.startsWith('Bearer')
        ) {
            token = req.headers.authorization.split(' ')[1];
        }
        if (!token) {
            return next(
                new AppError(
                    'You are not logged in! Please login to continue!',
                    401
                )
            );
        }

        //Verification of token
        const decoded = await util.promisify(jwt.verify)(
            token,
            process.env.JWT_SECRET
        );

        //Check if user still exists
        const currentUser = await User.findById(decoded.id);
        if (!currentUser) {
            return next(
                new AppError(
                    'The user belonging to the token no longer exists!',
                    404
                )
            );
        }
        //Password changed after token was issued
        if (currentUser.changedPasswordAfter(decoded.iat)) {
            return next(
                new AppError(
                    'The password was changed after the token was issued! Please log in again.',
                    401
                )
            );
        }

        //GRANT ACCESS TO PROTECTED ROUTE
        req.user = currentUser;
        next();
    } catch (err) {
        if (err.name === 'JsonWebTokenError') {
            next(new AppError('JWT Token not verified', 401));
        }
        if (err.name === 'TokenExpiredError') {
            next(new AppError('Token expired', 401));
        }
    }
};

exports.restrictTo = (...roles) => {
    return (req, res, next) => {
        if (!roles.includes(req.user.role)) {
            return next(
                new AppError(
                    'You do not have permission to perform this action!',
                    403
                )
            );
        }
        next();
    };
};

exports.forgotPassword = async (req, res, next) => {
    try {
        //1) Get user based on POSTed email
        const user = await User.findOne({ email: req.body.email });
        if (!user) {
            return next(new AppError('No user found with this email ID', 404));
        }

        //2) Generate random token and
        const resetToken = user.createPasswordResetToken();
        await user.save({ validateBeforeSave: false });

        //3) Send it to the user's email
        const resetURL = `${req.protocol}://${req.get(
            'host'
        )}/api/v1/users/resetPassword/${resetToken}`;

        const message = `Forgot your password? Submit a PATCH request with your new password and passwordConfirm to: ${resetURL}.\nIf you didn't forget your password please ignore this email.`;

        await sendEmail({
            email: user.email,
            subject: 'Your password reset link (Valid for 10 minutes)',
            message: message,
        });

        res.status(200).json({
            status: 'success',
            message: 'Token sent to email.',
        });
    } catch (err) {
        user.passwordResetToken = undefined;
        user.passwordResetExpires = undefined;
        await user.save({ validateBeforeSave: false });

        return next(new AppError('The email could not be sent', 500));
    }
};

exports.resetPassword = async (req, res, next) => {
    try {
        //1) Get user based on token
        const hashedToken = crypto
            .createHash('sha256')
            .update(req.params.token)
            .digest('hex');

        const user = await User.findOne({
            passwordResetToken: hashedToken,
            passwordResetExpires: { $gt: Date.now() },
        });

        //2) Set new password if token hasn't expired and user still exists
        if (!user) {
            return next(new AppError('Token is invalid or has expired', 400));
        }

        //3) Update changedPasswordAt property
        user.password = req.body.password;
        user.passwordConfirm = req.body.passwordConfirm;
        user.passwordResetToken = undefined;
        user.passwordResetExpires = undefined;
        await user.save();
        //4) Log the user in => Send jwt token
        createSendToken(user, 200, res);
    } catch (err) {
        const errors = Object.values(err.errors).map((el) => el.message);

        const message = `Invalid input data: ${errors.join(',')}`;
        return next(new AppError(message, 400));
    }
};

exports.changePassword = async (req, res, next) => {
    try {
        const user = await User.findById(req.user.id).select('+password');

        if (
            !(await user.correctPassword(
                req.body.currentPassword,
                user.password
            ))
        ) {
            return next(new AppError('Current password is incorrect', 401));
        }

        user.password = req.body.password;
        user.passwordConfirm = req.body.passwordConfirm;
        await user.save();
        console.log('1');
        createSendToken(user, 200, res);
    } catch (err) {
        const errors = Object.values(err.errors).map((el) => el.message);

        const message = `Invalid input data: ${errors.join(',')}`;
        return next(new AppError(message, 400));
    }
};
