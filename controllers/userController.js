const User = require('./../models/userModel');
const AppError = require('./../utils/appError');

const filterObj = (obj, ...allowedFields) => {
    const newObj = {};
    Object.keys(obj).forEach((el) => {
        if (allowedFields.includes(el)) newObj[el] = obj[el];
    });

    return newObj;
};

exports.getAllUsers = async (req, res, next) => {
    try {
        const users = await User.find();

        res.status(200).json({
            status: 'success',
            results: users.length,
            data: {
                users: users,
            },
        });
    } catch (err) {
        next(new AppError(`${err}`, 404));
    }
};

exports.viewProfile = async (req, res, next) => {
    try {
        const currentUser = await User.findById(req.user.id).select('name email');
        res.status(200).json({
            status: 'success',
            data: {
                user: currentUser,
            },
        });
    } catch (err) {
        next(new AppError(`${err}`, 404));
    }
};

exports.updateProfile = async (req, res, next) => {
    try {
        if (req.body.password || req.body.passwordConfirm) {
            return next(
                new AppError(
                    'Password updation not possible on this route',
                    400
                )
            );
        }

        const filteredBody = filterObj(req.body, 'name', 'email');

        const updatedUser = await User.findByIdAndUpdate(
            req.user.id,
            filteredBody,
            {
                new: true,
                runValidators: true,
            }
        );

        res.status(200).json({
            status: 'success',
            data: {
                user: updatedUser,
            },
        });
    } catch (err) {
        return next(new AppError('Information not updated', 400));
    }
};
