const Blog = require('./../models/blogModel');
const AppError = require('./../utils/appError');

const filterObj = (obj, ...allowedFields) => {
    const newObj = {};
    Object.keys(obj).forEach((el) => {
        if (allowedFields.includes(el)) newObj[el] = obj[el];
    });

    return newObj;
};

exports.getAllBlogs = async (req, res) => {
    try {
        const blogs = await Blog.find();
        res.status(200).json({
            status: 'success',
            results: blogs.length,
            data: {
                blogs: blogs,
            },
        });
    } catch (err) {
        res.status(404).json({
            status: 'fail',
            message: err,
        });
    }
};

exports.getBlog = async (req, res, next) => {
    try {
        const blog = await Blog.findById(req.params.id);

        if (!blog) {
            return next(new AppError(`No blog found with that ID`, 404));
        }

        res.status(200).json({
            status: 'success',
            data: {
                blog: blog,
            },
        });
    } catch (err) {
        res.status(404).json({
            status: 'fail',
            message: `${err}`,
        });
    }
};

exports.getUserBlogs = async (req, res) => {
    try {
        const userID = req.user.id;
        const blogs = await Blog.find({ createdBy: userID });

        res.status(200).json({
            status: 'success',
            data: {
                blogs: blogs,
            },
        });
    } catch (err) {
        res.status(404).json({
            status: 'fail',
            message: `${err}`,
        });
    }
};

exports.createBlog = async (req, res) => {
    try {
        const newBlog = await Blog.create({
            title: req.body.title,
            description: req.body.description,
            createdBy: req.user.id,
        });

        res.status(201).json({
            status: 'success',
            data: {
                blog: newBlog,
            },
        });
    } catch (err) {
        res.status(400).json({
            status: 'fail',
            message: err.message,
        });
    }
};

exports.updateBlog = async (req, res, next) => {
    try {
        const blogTemp = await Blog.findById(req.params.id);

        if (blogTemp.createdBy !== req.user.id)
            return next(new AppError(`Blog doesn't belong to you`, 403));

        const filteredBody = filterObj(req.body, 'title', 'description');
        const blog = await Blog.findByIdAndUpdate(req.params.id, filteredBody, {
            new: true,
            runValidators: true,
        });

        if (!blog) {
            return next(new AppError(`No blog found with that ID`, 404));
        }

        res.status(200).json({
            status: 'success',
            data: {
                blog: blog,
            },
        });
    } catch (err) {
        res.status(400).json({
            status: 'fail',
            message: `${err}`,
        });
    }
};

exports.deleteBlog = async (req, res, next) => {
    try {
        if (req.user.role === 'admin') {
            const blog = await Blog.findByIdAndDelete(req.params.id);

            if (!blog)
                return next(new AppError(`No blog found with that ID`, 404));
        }
        if (req.user.role === 'user') {
            const blog = await Blog.findById(req.params.id);

            if (!blog)
                return next(new AppError(`No blog found with that ID`, 404));

            if (blog.createdBy !== req.user.id) {
                return next(new AppError("Blog doesn't belong to you", 403));
            }
            blog.active = false;
            await blog.save();
        }

        res.status(204).json({
            status: 'success',
            data: null,
        });
    } catch (err) {
        res.status(400).json({
            status: 'fail',
            message: `${err}`,
        });
    }
};
